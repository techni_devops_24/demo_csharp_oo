﻿namespace _03_Classes_Props.Classes
{
    public class Chien
    {
        // ------ champs
        // Souvent privées, nous servent à accéder aux valeurs dans la classe
        private string _name;
        //private string _race;
        private DateOnly _birthdate;

        // ------ propriétés
        // Nous servent à définir le niveau d'accès et rajouter des conditions
        public string Name
        {
            //Lecture
            get { return _name; }

            //Ecriture
            /*private*/ set {
                if(value != "Pouet")
                {
                    _name = value;
                }
            }
        }
        
        //Si pas de restriction d'accès, on peut faire juste une propriété
        public string Race { get; set; }

        // Petit tips : prop + tab -> Créé une propriété pré-remplie

        // ------ constructeur(s)


        //méthodes
    }
}
