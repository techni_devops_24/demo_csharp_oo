﻿using _04_Indexeurs.Classes;

Chien chien1 = new Chien();
chien1.Name = "Rantanplan";
chien1.Race = "Croisé";

Guid guid1 = Guid.NewGuid();
Guid guid2 = Guid.NewGuid();

//Ajout indexeur 
chien1[guid1] = "Un os qui fait pouet";
chien1[guid2] = "Une belle balle de tennis";

//Récuper indexeur
Console.WriteLine("Ses jouet prefs :");
Console.WriteLine(chien1[guid1]);
Console.WriteLine(chien1[guid2]);