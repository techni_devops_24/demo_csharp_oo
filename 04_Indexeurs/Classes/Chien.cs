﻿namespace _04_Indexeurs.Classes
{
    public class Chien
    {
        //Props
        public string Name { get; set; }
        public string Race { get; set; }

        private Dictionary<Guid, string> _jouetsFav = new Dictionary<Guid, string>();

        //Indexeur
        public string this[Guid key]
        {
            get
            {
                //On renvoie la valeur à la clef passée dans les crochets
                _jouetsFav.TryGetValue(key, out string jouet);
                return jouet;
            }
            set
            {
                _jouetsFav[key] = value;
            }
        }

    }
}
