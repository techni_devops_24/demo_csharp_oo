﻿using _06_Heritage.Classes;

Chien chien1 = new Chien()
{
    Nom = "Taylor",
    Race = "Corgi",
    Metier = "Chômeur"
};

Chien chien2 = new Chien()
{
    Nom = "Skye",
    Race = "Lévrier whippet"
};

Guid guid1 = Guid.NewGuid();
Guid guid2 = Guid.NewGuid();

//Ajout indexeur 
chien2[guid1] = "Un os qui fait pouet";
chien2[guid2] = "Une belle balle de tennis";
Console.WriteLine("Les jouet prefs de " + chien2.Nom);
Console.WriteLine(chien2[guid1]);
Console.WriteLine(chien2[guid2]);

Chat chat = new Chat()
{
    Nom = "Garfield",
    Race = "Chat de gouttière"
};

Guid guid3 = Guid.NewGuid();
Guid guid4 = Guid.NewGuid();

//Ajout indexeur 
chat[guid3] = "Un plumeau";
chat[guid4] = "Une belle boîte en carton";
Console.WriteLine("Les jouet prefs de " + chat.Nom);
Console.WriteLine(chat[guid3]);
Console.WriteLine(chat[guid4]);

Chien bebeChien = chien1 + chien2;
Console.WriteLine($"Un bébé chien est né ! Son nom {bebeChien.Nom} et sa race {bebeChien.Race} ");

int nbMorts = chien1 + chat;
Console.WriteLine($"Il y a eu {nbMorts} mort(s) !");

chien1.Vacciner();
chat.Vacciner();

chien1.FaireDuBruit();
chat.FaireDuBruit();
Animal animal = new Chien();
animal.FaireDuBruit();

Console.WriteLine("--------------------");
Console.WriteLine();
chien1.Display();
Console.WriteLine();
chat.Display();


// Petit glossaire :

// Classe Dérivée -> Classe enfant (Chien)
// Classe de Base -> Classe parent (Animal)
// : -> Héritage
// Classe Dérivée : Classe de Base (Chien : Animal)
// Un chien hérite de animal
// Lorsqu'on fait un héritage, la classe dérivée hérite de tout ce qui se trouve dans la classe de base (sauf constructeurs et destructeurs)

// Dans le cas où une méthode/propriété est présente dans chaque classe mais à un comportement/une valeur différent
// virtual [Sur la classe de base]
// -> Permet de dire que la méthode/propriété peut être réécrite dans l'enfant
// override [Sur les classes dérivées]
// -> Permet de réécrire la mathode/propriété pour remplacer celle de la classe parent
// Ex Méthode Parent
// public virtual void FaireDuBruit()
// Ex Méthode Enfant
// public override void FaireDuBruit()
// Ex Prop Parent
// public virtual string Cri { get {}}
// Ex Prop Enfant
// public override string Cri { get {}}

// Si on veut appeler une méthode/propriété qui se trouve dans la classe parent
// base -> représente la classe parent (classe de base)
// base.Display()
