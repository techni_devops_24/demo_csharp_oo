﻿namespace _06_Heritage.Classes
{
    public class Animal
    {
        public string Nom { get; set; }
        public string Race { get; set; }

        public virtual string Cri { get { return "Ouga Ouga"; } }

        private Dictionary<Guid, string> _jouetsFav = new Dictionary<Guid, string>();

        //Indexeur
        public string this[Guid key]
        {
            get
            {
                //On renvoie la valeur à la clef passée dans les crochets
                _jouetsFav.TryGetValue(key, out string jouet);
                return jouet;
            }
            set
            {
                _jouetsFav[key] = value;
            }
        }

        public void Vacciner()
        {
            Console.WriteLine($"{Nom} vient de se faire vacciner");
        }

        //virtual : Permet de définir que cette méthode peut être réécrite dans les enfants pour avoir un comportement différent
        public virtual void FaireDuBruit()
        {
            Console.WriteLine("Ouga Ouga");
        }

        public virtual void Display()
        {
            Console.WriteLine($"Voici {Nom} de race {Race}");
        }
    }
}
