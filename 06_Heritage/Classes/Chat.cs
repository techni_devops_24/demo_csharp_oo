﻿
namespace _06_Heritage.Classes
{
    public class Chat : Animal
    {
        public string CachetteFavorite { get; set; }

        //override : Redéfinition de la méthode FaireDuBruit qui se trouve dans Animal, pour avoir CE ↓ comportement là plutôt que celui de Animal
        public override void FaireDuBruit()
        {
            Console.WriteLine("Miaou Miaou");
        }
    }
}
