﻿namespace _06_Heritage.Classes
{
    public sealed class Chien : Animal
    {
        //Props
        // Grâce à l'héritage (: Animal), on a d'office les props Nom et Race qui se trouvent dans Animal
        public string Metier { get; set; }
        public override string Cri { get { return "Waf Waf"; } }

        //Surcharge Opérateur(s)
        public static Chien operator +(Chien ch1, Chien ch2)
        {
            Chien bebe = new Chien
            {
                Nom = $"Bébé de {ch1.Nom} et {ch2.Nom}",
                Race = $"Croisé {ch1.Race} et {ch2.Race}",
            };
            return bebe;
        }

        public static int operator +(Chien waf, Chat miaou)
        {
            Console.WriteLine($"C'est la bagarre entre {waf.Nom} et {miaou.Nom}");
            Random random = new Random();
            int resultat = random.Next(1, 5);
            switch (resultat)
            {
                case 1:
                    Console.WriteLine("Le chien a gagné");
                    return 1;
                case 2:
                    Console.WriteLine("Le chat a gagné");
                    return 1;
                case 3:
                    Console.WriteLine("Les deux se sont entretués");
                    return 2;
                case 4:
                    Console.WriteLine("Les deux sont gigas nuls");
                    return 0;
                default:
                    Console.WriteLine("Erreur dans la génération de nombre");
                    return 0;
            }

        }

        //override : Redéfinition de la méthode FaireDuBruit qui se trouve dans Animal, pour avoir CE ↓ comportement là plutôt que celui de Animal
        public override void FaireDuBruit()
        {
            Console.WriteLine("Waf Waf");
        }
        public override void Display()
        {
            //base -> Représente la classe parent
            //base.Display() -> Va appeler la méthode Display qui est dans le parent
            base.Display();
            Console.WriteLine($"Dans la vie je suis un super chien {Metier}");

        }
    }

    //Impossible car avec le rajoute de sealed sur la classe Chien, rien ne peut hériter de cette classe (elle est scellée)
    //public class SousChien : Chien 
    //{ 

    //}
}
