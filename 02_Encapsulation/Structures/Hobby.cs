﻿namespace _02_Encapsulation.Structures
{
    public struct Hobby
    {
        public string Name;
        private string Description;

        public void ShowHobby()
        {
            Console.WriteLine($"{Name} - {Description}");
        }

    }
}
