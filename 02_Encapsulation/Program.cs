﻿using _02_Encapsulation.Structures;

Hobby hobby1;
hobby1.Name = "Twitch"; 
//public -> Droit d'y accéder partout
//hobby1.Description = "Regarder des streams Art"; 
//private -> Droit d'accès que dans le type, donc pas ici
//protected -> Droit d'accès dans la classe + enfants