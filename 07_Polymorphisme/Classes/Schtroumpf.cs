﻿
//Dossier "logique" -> Schtroumpf se trouve dans le "dossier" Classes, lui même dans un "dossier" _00_Recap
namespace _07_Polymorphisme.Classes
{
    public class Schtroumpf
    {
        // Champs
        private string _couleur = "Bleu";
        private bool _mordu;
        private Dictionary<int, string> _blablaNormal = new Dictionary<int, string>()
        {
            { 0 , "Il fait schtroumpfement beau aujourd'hui" },
            { 1 , "Schtroumpf ! J'ai marché dans une schtroumpf !" },
            { 2 , "J'ai préparé un délicieux schtroumpf à la salsepareille" },
            { 3 , "Schtroumpf alors ! Voilà Gargamel !" },
        };

        private Dictionary<int, string> _blablaNoir = new Dictionary<int, string>()
        {
            { 0 , "Gnap Gnap Gnap Gnap Gnap" },
            { 1 , "Gnap ! Gnap Gnap Gnap Gnap Gnap !" },
            { 2 , "Gnap Gnap Gnap Gnap Gnap Gnap Gnap Gnap" },
            { 3 , "Gnap Gnap ! Gnap Gnap !" },
        };


        // Propriété
        public virtual string Couleur {
            get
            {
                return _couleur;
            }
            private set //lecture seule
            {
                if (_mordu)
                {
                    _couleur = value;
                }
            }
        }

        public virtual string Genre { get { return "male"; } } //Ajout d'une prop auto Genre pour pouvoir override dans les enfants si pas male
       
        public virtual bool Malade { get 
            {
                return _mordu;
            }
            private set 
            { 
                _mordu = true;
            } 
        }
        // Auto-Propriété
        public virtual string Nom { get; private set; } //Tous les Schtroumpfs ont un nom mais on doit pouvoir le redéfinir dans chaque enfant

        // Indexeur
        public string this[int index]
        {
            get
            {
                if (_mordu)
                {
                    _blablaNoir.TryGetValue(index, out string phrase);
                    return phrase ?? "Gnap !";
                    //Si getValue échoue, phrase est null donc on utilise le coalesce pour renvoyer une phrase par défaut
                }
                else
                {
                    _blablaNormal.TryGetValue(index, out string phrase);
                    return phrase ?? "Schtroumpf alors !";
                }
            }
            set
            {
                if (_mordu)
                {
                    _blablaNoir[index] = value;
                }
                else
                {
                    _blablaNormal[index] = value;
                }
            }
        }

        // Constructeur (prématurément)

        // Surcharge opérateur
        public static BebeSchtroumpf? operator +(Schtroumpf s1, Schtroumpf s2){
            if (s1.Genre == s2.Genre)
            {
                Console.WriteLine($"{s1.Nom} et {s2.Nom} y'a un problème biologique là en fait, il vous faut de l'aide pour avoir un p'tit bébé schtroumpf");
                return null;
            }
            Console.WriteLine($"{s1.Nom} et {s2.Nom} schtroumpfent joyeusement !");
            BebeSchtroumpf bebe = new BebeSchtroumpf();
            Console.WriteLine($"{bebe.Nom} est né{ (bebe.Genre == "male" ? "" : "e") } !");
            return bebe;
        }

         // Méthode
        public void SeFaireMordre(Schtroumpf schtroumpfMordeur)
        {
            Console.WriteLine();
            Console.WriteLine($"{schtroumpfMordeur.Nom} mord {Nom} !");
            if (schtroumpfMordeur.Malade)
            {
                _mordu = true;
                Couleur = "Noir";
                Console.WriteLine($"Oh non {Nom} est devenu tout noir");
            }
            else
            {
                Console.WriteLine("Ca n'a aucun effet");
            }
        }

        public virtual void Presenter() //à redéfinir dans chacun des schtroumpfs
        {
            Console.WriteLine($"Voici {Nom} de couleur {Couleur} ");
           
        }
        // surcharge de méthode
        public void Presenter(string lang)
        {
            switch(lang)
            {
                case "fr":
                    Console.WriteLine($"Voici {Nom}");
                    break;
                case "en":
                    Console.WriteLine($"Here is {Nom}");
                    break;
                case "es":
                    Console.WriteLine($"Esta {Nom}");
                    break;
                default:
                    Console.WriteLine("Langue inconnue");
                    break;
            }
        }
    }
}
