﻿
namespace _07_Polymorphisme.Classes
{
    public class SchtroumpfNoir : Schtroumpf
    {
        public override string Nom { get { return "Le Schtroumpf Noir"; } }

        public override string Couleur { get { return "Noir"; } }

        public override bool Malade { get { return true; } }
        public override void Presenter()
        {
            Console.WriteLine($"Gnap Gnap, gnap gnap {Nom} gnap gnap gnap {Couleur}");
        }
    }
}
