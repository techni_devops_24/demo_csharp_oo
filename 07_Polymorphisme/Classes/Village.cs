﻿
namespace _07_Polymorphisme.Classes
{
    public class Village
    {
        public string Nom { get; set; }

        private Dictionary<string, Schtroumpf> _habitants = new Dictionary<string, Schtroumpf >();

        //indexeur
        public Schtroumpf? this[string nom]
        {
            get 
            { 
                _habitants.TryGetValue(nom, out Schtroumpf? schtroumpf); 
                return schtroumpf;
            }
            set 
            {
                if(value is not null)
                {
                    _habitants[nom] = value;
                }
            }
        }

        public void AjouterUnSchtroumpf(Schtroumpf schtroumpf)
        {
            Console.WriteLine("Bienvenue au village " + schtroumpf.Nom);
            _habitants.Add(schtroumpf.Nom, schtroumpf);
        }
    }
}
