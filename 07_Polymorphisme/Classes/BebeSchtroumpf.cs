﻿namespace _07_Polymorphisme.Classes
{
    public class BebeSchtroumpf : Schtroumpf
    {
        private int rand = new Random().Next(1, 3);
        public override string Nom { get { return "Bébé Schtroumpf"; } }
        public override string Genre { 
            get 
            {
                return rand == 1 ? "male" : "femelle"; 
            } 
        }

        public override void Presenter()
        {
            Console.WriteLine($"Agougou, agouga {Nom} gaga {Couleur}");
        }
    }
}
