﻿using EspaceDeNom.Enums;
using EspaceDeNom.Structures;

using Drow = EspaceDeNom.Mocrisift.Drow;
using Lexce = EspaceDeNom.Mocrisift.Lexce;

User aurelien = new User()
{
    FirstName = "Aurélien",
    LastName = "Strimelle",
    Role = RoleEnum.Admin
};

// Comme on a importé EspaceDeNom.Structures, il a pris celui dans le dossier Structures + celui dans le fichier Pouet.cs et nous avons donc accès à Hobby aussi
Hobby hobby = new Hobby()
{
    Name = "Manger",
    Category = "Boring"
};

//Sans Alias -> Relou
// Avec alias (voir usings tout en haut)
//using Drow = EspaceDeNom.Mocrisift.Drow;
//using Lexce = EspaceDeNom.Mocrisift.Lexce;

//EspaceDeNom.Mocrisift.Drow.Document docDrow = new EspaceDeNom.Mocrisift.Drow.Document()
//{
//    Name = "Lettre de motivation",
//    Description = "Jve travay pliz"
//}; 

Drow.Document docDrow = new Drow.Document()
{
    Name = "Lettre de motivation",
    Description = "Jve travay pliz"
};

//EspaceDeNom.Mocrisift.Lexce.Document docLexce = new EspaceDeNom.Mocrisift.Lexce.Document()
//{
//    Name = "Frais kilométriques Mars",
//    Description = "Ah bah Liège ca fait loin"
//};

Lexce.Document docLexce = new Lexce.Document()
{
    Name = "Frais kilométriques Mars",
    Description = "Ah bah Liège ca fait loin"
};