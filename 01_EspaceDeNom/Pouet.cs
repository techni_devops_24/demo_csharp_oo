﻿
// Rarement utilisé mais c'est possible de le faire
namespace EspaceDeNom
{
    // C'est comme si on faisait une arborescence logique mais sans représentation physique (dossiers)
    // C'est l'enfer pour trouver le bon fichier/type à modifier
    namespace Structures
    {
        public struct Hobby
        {
            public string Name;
            public string Category;
        }
    }

    namespace Enums
    {

    }
}
