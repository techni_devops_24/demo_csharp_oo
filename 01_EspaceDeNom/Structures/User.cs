﻿
using EspaceDeNom.Enums; //Import du dossier Enums

namespace EspaceDeNom.Structures
{
    public struct User
    {
        public string LastName;
        public string FirstName;
        public RoleEnum Role;
    }
}
