﻿using _09_Static.Classes;

/*InfoSchtroumpf iS = new InfoSchtroumpf();*/ //Interdit vu que InfoSchtroumpf est static

// Par exemple Math est une classe statique :
Console.WriteLine("La constante de PI vaut " + Math.PI);
Console.WriteLine(Math.Pow(2, 7));

Console.WriteLine("Voici la liste des auteurs des Schtroumpfs");
foreach(string auteur in InfoSchtroumpf.Auteurs)
{
    Console.WriteLine(auteur);
}

InfoSchtroumpf.ParlerLeSchtroumpf();

Schtroumpf schtroumpfALunettes = new Schtroumpf() { Name = "Schtroumpf à lunettes" };
Schtroumpf schtroumpFarceur = new Schtroumpf() { Name = "Schtroumpf farceur" };

Console.WriteLine($"Il y a en tout {Schtroumpf.NombreSchtroumpf} schtroumpfs");