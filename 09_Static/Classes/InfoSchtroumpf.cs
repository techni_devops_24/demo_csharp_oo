﻿using System.Security.Claims;

namespace _09_Static.Classes
{
    static class InfoSchtroumpf
    {
        public static List<string> Auteurs { get; set; } = new List<string>()
        {
             "Yvan Delporte", 
            "Peyo",
            "Gos",
            "Thierry Culliford",
            "Alain Maury",
            "Luc Parthoens",
            "Philippe Delzenne",
            "Pascal Garray",
            "Alain Jost",
            "Jeroen De Coninck",
            "Nine",
            "Miguel Díaz Vizoso",
            "Peral"
        };

        public static DateOnly PremiereParution { get; set; } = new DateOnly(1958, 10, 23);
        public static List<string> DixPremiersTomes { get; set; } = new List<string>()
        {
            "La Flûte à six trous",
            "La Guerre des sept fontaines",
            "Les Schtroumpfs noirs",
            "Le Voleur de Schtroumpfs",
            "L'Œuf et les Schtroumpfs",
            "Le Faux Schtroumpf",
            "La Faim des Schtroumpfs",
            "Le Pays maudit",
            "Le Centième Schtroumpf",
            "Le Schtroumpf volant" 
        };

        public static void ParlerLeSchtroumpf()
        {
            Console.WriteLine("Schtroumpf alors !");
        }
    }
}
