﻿namespace _10_Interfaces.Interfaces
{
    public interface IHerbivore
    {
        public bool CarenceB12 { get;}

        public void MangerDeLHerbeEtDesOeufs();
        
    }
}
