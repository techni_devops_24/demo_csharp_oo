﻿namespace _10_Interfaces.Interfaces
{
    public interface ICarnivore
    {
        public bool CancerColon { get;}
        public void MangerQueDLaViande();
    }
}
