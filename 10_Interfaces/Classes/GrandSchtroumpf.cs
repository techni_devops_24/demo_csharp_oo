﻿using _10_Interfaces.Interfaces;

namespace _10_Interface.Classes
{
    public class GrandSchtroumpf : Schtroumpf, ICarnivore
    {
        public override string Nom { get { return "Le Grand Schtroumpf"; } }

        public bool CancerColon { get { return true; } }

        public override void Presenter()
        {
            Console.WriteLine($"Kof Kof, c'est moi {Nom} et je suis {Couleur}");
        }
        
        public void JeSuisQui()
        {
            Console.WriteLine("Je suis le chef");
        }

        public void MangerQueDLaViande()
        {
            Console.WriteLine("Azraël était délicieux au Barbeuq");
        }
    }
}
