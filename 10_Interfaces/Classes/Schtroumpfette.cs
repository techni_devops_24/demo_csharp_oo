﻿using _10_Interfaces.Interfaces;

namespace _10_Interface.Classes
{
    public class Schtroumpfette : Schtroumpf, IHerbivore
    {
        public override string Nom { get { return "La Schtroumpfette"; } }
        public override string Genre { get { return "femelle"; } }

        public bool CarenceB12 { get { return false; } }

        public void MangerDeLHerbeEtDesOeufs()
        {
            Console.WriteLine("Hihi j'adore schtroumpfer de l'herbe et des oeufs et du fromage");
        }

        public override void Presenter()
        {
            Console.WriteLine($"Hihi, c'est moi {Nom} et je suis {Couleur}e");
        }
    }
}
