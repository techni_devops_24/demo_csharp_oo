﻿using _08_Abstract.Classes;

// Le polymorphisme, c'est considérer qu'une classe a le type de sa classe + celui de tous les parents
// La Schtroumpfette est à la la fois une Schtroumpfette, un Schtroumpf et un Object

Village Village = new Village()
{
    Nom = "Village des Schtroumpfs"
};

// Casting implicite AjouterUnSchtroumpf s'attend à recevoir un Schtroumpf et on lui donne un enfant de Schtroumpf
// Schtroumpf monSchtroumpf = new Schtroumpfette(); //<- Casting implicite
Village.AjouterUnSchtroumpf(new Schtroumpfette());
Village.AjouterUnSchtroumpf(new GrandSchtroumpf());
Village.AjouterUnSchtroumpf(new SchtroumpfNoir());

Console.WriteLine();
// vu que Présenter() était dans Schtroumpf, on y a accès
// par contre comme il est override dans chaque enfant, c'est le bon Présenter() qui sera appelé
Village["La Schtroumpfette"]?.Presenter();
Village["Le Grand Schtroumpf"]?.Presenter();
Village["Le Schtroumpf Noir"]?.Presenter();

//Casting explicite
//Schtroumpf sch = new GrandSchtroumpf();
//sch.JeSuisQui() //Pas dispo
//((GrandSchtroumpf)sch).JeSuisQui(); //Dispo
((GrandSchtroumpf)Village["Le Grand Schtroumpf"])?.JeSuisQui();

// -------- ABSTRACTION
Schtroumpf sch1 = new Schtroumpfette(); // On veut pouvoir utiliser les avantage du polymorphisme
//Schtroumpf sch2 = new Schtroumpf(); // Mais on veut ne pas pouvoir créer un Schtroumpf






