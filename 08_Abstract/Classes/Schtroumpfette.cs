﻿namespace _08_Abstract.Classes
{
    public class Schtroumpfette : Schtroumpf
    {
        public override string Nom { get { return "La Schtroumpfette"; } }
        public override string Genre { get { return "femelle"; } }

        public override void Presenter()
        {
            Console.WriteLine($"Hihi, c'est moi {Nom} et je suis {Couleur}e");
        }
    }
}
