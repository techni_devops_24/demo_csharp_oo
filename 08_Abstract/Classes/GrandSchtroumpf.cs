﻿namespace _08_Abstract.Classes
{
    public class GrandSchtroumpf : Schtroumpf
    {
        public override string Nom { get { return "Le Grand Schtroumpf"; } }
        public override void Presenter()
        {
            Console.WriteLine($"Kof Kof, c'est moi {Nom} et je suis {Couleur}");
        }
        
        public void JeSuisQui()
        {
            Console.WriteLine("Je suis le chef");
        }
    }
}
