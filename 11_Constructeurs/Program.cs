﻿using _11_Constructeurs.Classes;
using _11_Constructeurs.Interfaces;

// Le polymorphisme, c'est considérer qu'une classe a le type de sa classe + celui de tous les parents
// La Schtroumpfette est à la la fois une Schtroumpfette, un Schtroumpf et un Object

Village Village = new Village()
{
    Nom = "Village des Schtroumpfs"
};

// Casting implicite AjouterUnSchtroumpf s'attend à recevoir un Schtroumpf et on lui donne un enfant de Schtroumpf
// Schtroumpf monSchtroumpf = new Schtroumpfette(); //<- Casting implicite
Village.AjouterUnSchtroumpf(new Schtroumpfette());
Village.AjouterUnSchtroumpf(new GrandSchtroumpf());
Village.AjouterUnSchtroumpf(new SchtroumpfNoir());

Console.WriteLine();
// vu que Présenter() était dans Schtroumpf, on y a accès
// par contre comme il est override dans chaque enfant, c'est le bon Présenter() qui sera appelé
Village["La Schtroumpfette"]?.Presenter();
Village["Le Grand Schtroumpf"]?.Presenter();
Village["Le Schtroumpf Noir"]?.Presenter();

//Casting explicite
//Schtroumpf sch = new GrandSchtroumpf();
//sch.JeSuisQui() //Pas dispo
//((GrandSchtroumpf)sch).JeSuisQui(); //Dispo
((GrandSchtroumpf)Village["Le Grand Schtroumpf"])?.JeSuisQui();

// -------- ABSTRACTION
Schtroumpf sch1 = new Schtroumpfette(); // On veut pouvoir utiliser les avantage du polymorphisme
//Schtroumpf sch2 = new Schtroumpf(); // Mais on veut ne pas pouvoir créer un Schtroumpf

//Schtroumpf? schtroumpfATable = Village["La Schtroumpfette"];
//Schtroumpf? schtroumpfATable = Village["Le Grand Schtroumpf"];
Schtroumpf? schtroumpfATable = Village["Le Schtroumpf Noir"];

if (schtroumpfATable is IHerbivore sch)
{
    //IHerbivore sch = (IHerbivore)schtroumpfATable;
    sch.MangerDeLHerbeEtDesOeufs();
}
if (schtroumpfATable is ICarnivore sch2)
{
    //ICarnivore sch2 = (ICarnivore)schtroumpfATable;
    sch2.MangerQueDLaViande();
}

Dictionary<int, string> phrasesSchtroumpfette = new Dictionary<int, string>()
        {
            { 0 , "Hihi j'ai cueilli des schtroumpfs" },
            { 1 , "J'ai des beaux schtroumpfs blonds" },
            { 2 , "Oups, le vent a schtroumpfé ma robe !" },
            { 3 , "J'ai mal aux schtroumpfs" },
        };
// Désactiver abstract pour faire marcher constructeur de Schtroumpf 
// Schtroumpf schAgain = new Schtroumpf();
// Schtroumpf schAgain = new Schtroumpf(phrasesBase);

// Attention à l'ordre d'appel des constructeurs lors de l'héritage.
// Quand on appelle le constructeur de Schtroumpfette :
// 1 : celui ci appelle le constructeur de Schtroumpf sans traiter ce qu'il y a dans le constructeur Schtroumpfette. 
// 2 : le constrcuteur Schtroumpf fait appel au constructeur object sans traiter ce qu'il y a dans le constructeur de Schtroumpf
// 3 : Traitement de ce qu'il y a dans le constructeur de Object (vu que c'est le dernier au top de l'héritage)
// 4 : On revient traiter ce qu'il y a dans le constructeur de Schtroumpf
// 5 : On revient traiter ce qu'il y a dans le constructeur de Schtroumpfette
Schtroumpfette schette = new Schtroumpfette(phrasesSchtroumpfette);
GrandSchtroumpf grandsch = new GrandSchtroumpf();

Console.WriteLine("\nUne conversation entre deux Schtroumpfs par un bel après-midi de printemps :");
Console.WriteLine($"{grandsch.Nom} : {grandsch[0]}");
Console.WriteLine($"{schette.Nom} : {schette[0]}");
Console.WriteLine($"{grandsch.Nom} : {grandsch[1]}");
Console.WriteLine($"{schette.Nom} : {schette[1]}");

using (TextWriter file = File.CreateText("Du texte"))
{
    //L'objet file existe le temps du traitement entre accolades
    file.Close();
}
//file.Close();
//L'objet file n'est pas accessible une fois le bloc d'instructions du using passé
//Une petite étiquette "A poubeller" est collée dessus pour le Garbage Collector

// vous aller l'utiliser en WebAPI pour gérer vos connections à la DB


