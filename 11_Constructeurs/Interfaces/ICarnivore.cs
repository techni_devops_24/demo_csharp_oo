﻿namespace _11_Constructeurs.Interfaces
{
    public interface ICarnivore
    {
        public bool CancerColon { get;}
        public void MangerQueDLaViande();
    }
}
