﻿namespace _11_Constructeurs.Interfaces
{
    public interface IHerbivore
    {
        public bool CarenceB12 { get;}

        public void MangerDeLHerbeEtDesOeufs();
        
    }
}
