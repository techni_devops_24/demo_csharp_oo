﻿using _11_Constructeurs.Interfaces;

namespace _11_Constructeurs.Classes
{
    public class GrandSchtroumpf : Schtroumpf, ICarnivore
    {
        public override string Nom { get { return "Le Grand Schtroumpf"; } }

        public bool CancerColon { get { return true; } }

        public GrandSchtroumpf() : base()
        {

        }

        public GrandSchtroumpf(Dictionary<int, string> phrases) : base(phrases)
        {

        }

        public override void Presenter()
        {
            Console.WriteLine($"Kof Kof, c'est moi {Nom} et je suis {Couleur}");
        }
        
        public void JeSuisQui()
        {
            Console.WriteLine("Je suis le chef");
        }

        public void MangerQueDLaViande()
        {
            Console.WriteLine("Azraël était délicieux au Barbeuq");
        }
    }
}
