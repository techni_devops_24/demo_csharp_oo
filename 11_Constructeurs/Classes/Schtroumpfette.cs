﻿using _11_Constructeurs.Interfaces;

namespace _11_Constructeurs.Classes
{
    public class Schtroumpfette : Schtroumpf, IHerbivore
    {
        public override string Nom { get; protected set; }
        public override string Genre { get { return "femelle"; } }

        public bool CarenceB12 { get { return false; } }

        public Schtroumpfette() : base()
        {
            Nom = "La Schtroumpfette";
        }
        public Schtroumpfette(Dictionary<int, string> phrases) : base(phrases)
        {
            Nom = "La Schtroumpfette";
        }

        ~Schtroumpfette()
        {
            Console.WriteLine("Destruction de la Schtroumpfette :'(");
        }

        public void MangerDeLHerbeEtDesOeufs()
        {
            Console.WriteLine("Hihi j'adore schtroumpfer de l'herbe et des oeufs et du fromage");
        }

        public override void Presenter()
        {
            Console.WriteLine($"Hihi, c'est moi {Nom} et je suis {Couleur}e");
        }
    }
}
