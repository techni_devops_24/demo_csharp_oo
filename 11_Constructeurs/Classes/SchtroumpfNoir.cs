﻿
using _11_Constructeurs.Interfaces;

namespace _11_Constructeurs.Classes
{
    public class SchtroumpfNoir : Schtroumpf, IOmnivore
    {
        public override string Nom { get { return "Le Schtroumpf Noir"; } }

        public override string Couleur { get { return "Noir"; } }

        public override bool Malade { get { return true; } }

        public bool CancerColon => false;

        public bool CarenceB12 => true;

        public void MangerDeLHerbeEtDesOeufs()
        {
            Console.WriteLine("Gnap Gnap Herbe");
        }

        public void MangerQueDLaViande()
        {
            Console.WriteLine("Gnap Gnap Viande");
        }

        public override void Presenter()
        {
            Console.WriteLine($"Gnap Gnap, gnap gnap {Nom} gnap gnap gnap {Couleur}");
        }
    }
}
