﻿using _00_Recap.Classes;

//Création des schtroumpfs
Schtroumpfette schtroumpfette = new Schtroumpfette();
GrandSchtroumpf grandSchtroumpf = new GrandSchtroumpf();
SchtroumpfNoir schtroumpfNoir = new SchtroumpfNoir();

//Présentation de nos Schtroumpfs
schtroumpfette.Presenter();
schtroumpfette.Presenter("es");
grandSchtroumpf.Presenter();
schtroumpfNoir.Presenter();

schtroumpfette.SeFaireMordre(grandSchtroumpf);
schtroumpfette.Presenter();

schtroumpfette.SeFaireMordre(schtroumpfNoir);
schtroumpfette.Presenter();

// Utilisation de l'indexeur
Console.WriteLine("\nUne conversation entre deux Schtroumpfs par un bel après-midi de printemps :");
Console.WriteLine($"{grandSchtroumpf.Nom} : {grandSchtroumpf[0]}");
Console.WriteLine($"{schtroumpfette.Nom} : {schtroumpfette[0]}");
Console.WriteLine($"{grandSchtroumpf.Nom} : {grandSchtroumpf[1]}");
Console.WriteLine($"{schtroumpfette.Nom} : {schtroumpfette[1]}");

// Surcharge op
Console.WriteLine();
BebeSchtroumpf? testBebe = grandSchtroumpf + schtroumpfNoir;
Console.WriteLine();
BebeSchtroumpf? testBebe2 = grandSchtroumpf + schtroumpfette;




