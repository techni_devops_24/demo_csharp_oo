﻿namespace _05_SurchargeOperateur.Classes
{
    public class Chien
    {
        //Props
        public string Name { get; set; }
        public string Race { get; set; }

        private Dictionary<Guid, string> _jouetsFav = new Dictionary<Guid, string>();

        //Indexeur
        public string this[Guid key]
        {
            get
            {
                //On renvoie la valeur à la clef passée dans les crochets
                _jouetsFav.TryGetValue(key, out string jouet);
                return jouet;
            }
            set
            {
                _jouetsFav[key] = value;
            }
        }

        //Surcharge Opérateur(s)
        public static Chien operator +(Chien ch1, Chien ch2)
        {
            Chien bebe = new Chien
            {
                Name = $"Bébé de {ch1.Name} et {ch2.Name}",
                Race = $"Croisé {ch1.Race} et {ch2.Race}",
            };
            return bebe;
        }

        public static int operator +(Chien waf, Chat miaou)
        {
            Console.WriteLine($"C'est la bagarre entre {waf.Name} et {miaou.Nom}");
            Random random = new Random();
            int resultat = random.Next(1, 5);
            switch(resultat)
            {
                case 1:
                    Console.WriteLine("Le chien a gagné");
                    return 1;
                case 2:
                    Console.WriteLine("Le chat a gagné");
                    return 1;
                case 3:
                    Console.WriteLine("Les deux se sont entretués");
                    return 2;
                case 4:
                    Console.WriteLine("Les deux sont gigas nuls");
                    return 0;
                default:
                    Console.WriteLine("Erreur dans la génération de nombre");
                    return 0;
            }
          
        }

    }
}
