﻿using _05_SurchargeOperateur.Classes;

Chien chien1 = new Chien()
{
    Name = "Taylor",
    Race = "Corgi"
};

Chien chien2 = new Chien()
{
    Name = "Skye",
    Race = "Lévrier whippet"
};

Chat chat = new Chat()
{
    Nom = "Garfield"
};

Chien bebeChien = chien1 + chien2;
Console.WriteLine($"Un bébé chien est né ! Son nom {bebeChien.Name} et sa race {bebeChien.Race} ");

int nbMorts = chien1 + chat;
Console.WriteLine($"Il y a eu {nbMorts} mort(s) !");